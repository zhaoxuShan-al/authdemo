package com.tuling.security.distributed.salary.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import
        org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import
        org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;


import
        org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import
        org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import
        org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
public class MyResourceServerConfig extends ResourceServerConfigurerAdapter {
    public static final String RESOURCE_SALARY = "salary";

    @Autowired
    private TokenStore tokenStore;
    /**
     *ResourceServerSecurityConfigurer中主要包含：
     * tokenServices : ResourceServerTokenServices类的实例，用来实现令牌服
     * 务，即如何验证令牌。
     * tokenStore ： TokenStore类的实例，指定令牌如何访问，与tokenServices配
     * 置可选
     * resourceId ： 这个资源服务的ID，是可选的。但是推荐设置并在授权服务中进
     * 行验证。
     * 其他的扩展属性例如tokenExtractor令牌提取器用来提取请求中的令牌。
     * @param resources
     * @throws Exception
     */
    @Override
    public void configure(ResourceServerSecurityConfigurer resources)
            throws Exception {
        resources.resourceId(RESOURCE_SALARY) //资源ID
                //.tokenServices(tokenStore) //使用远程服务验证令牌的服务
                .tokenStore(tokenStore)
                .stateless(true); //无状态模式
    }

    /**
     * HttpSecurity，这个配置与Spring Security类似：
     * authorizeRequests()方法验证请求。antMatchers方法匹配访问路径。access()
     * 方法配置需要的权限。
     * .sessionManagement()方法配置session管理策略。
     * 其他自定义权限保护规则也通过HttpSecurity来配置
     * @param http
     * @throws Exception
     */
    //配置安全策略
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests() //校验请求
                .antMatchers("/salary/**") // 路径匹配规则。
                .access("#oauth2.hasScope('all')") // 需要匹配scope
                .and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        ;
    }
/*    //配置access_token远程验证策略。
    public ResourceServerTokenServices tokenServices(){
// DefaultTokenServices services = new DefaultTokenServices();
        RemoteTokenServices services = new RemoteTokenServices();
        services.setCheckTokenEndpointUrl("http://localhost:53020/uaa/oauth/check_token");
                services.setClientId("c1");
        services.setClientSecret("secret");
        return services;
    }*/
}