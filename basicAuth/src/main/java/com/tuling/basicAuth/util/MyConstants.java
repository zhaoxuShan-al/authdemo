package com.tuling.basicAuth.util;

/**
 * @Description
 * @Author al_tek@qq.com
 * @Date 2022/1/6 15:23
 */
public class MyConstants {

    public static final String FLAG_CURRENTUSER = "currnetUser";
    public static final String RESOURCE_COMMON = "common";
    public static final String RESOURCE_MOBILE = "mobile";
    public static final String RESOURCE_SALARY = "salary";
}
