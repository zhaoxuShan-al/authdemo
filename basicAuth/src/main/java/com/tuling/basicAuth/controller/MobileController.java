package com.tuling.basicAuth.controller;

/**
 * @Description
 * @Author al_tek@qq.com
 * @Date 2022/1/6 15:20
 */
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/mobile")
public class MobileController {

    @GetMapping("/query")
    public String query(){
        return "mobile";
    }

}