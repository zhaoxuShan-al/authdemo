package com.tuling.basicAuth.service;

/**
 * @Description
 * @Author al_tek@qq.com
 * @Date 2022/1/6 15:22
 */
import com.tuling.basicAuth.bean.UserBean;
import com.tuling.basicAuth.util.TestData;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.UUID;
@Service
public class AuthService {
    private final String demoUserName = "admin";
    private final String demoUserPass = "admin";

    @Resource
    private TestData testData;
    public UserBean userLogin(UserBean user){
        UserBean queryUser = testData.qeryUser(user);
        if(null != queryUser){
            queryUser.setUserId(UUID.randomUUID().toString());
        }
        return queryUser;
    }
}